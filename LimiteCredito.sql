/* Crear una tabla llamada ActualizacionLimiteCredito con 3 campos: Fecha, CodigoCliente, Incremento. Crea un procedimiento almacenado para actualizar los límites de crédito de los clientes en un % del total pedido entre 2008 y 2010 registrando el incremento en la tabla creada. Al terminar, invoca al procedimiento para actualizar el límite de crédito en un 15%.*/

USE Jardineria;

DROP PROCEDURE IF EXISTS crearCredito;

DELIMITER |

DROP TABLE IF EXISTS ActualizacionLimiteCredito;

CREATE PROCEDURE crearInfo()

  BEGIN

	CREATE TABLE ActualizacionLimiteCredito (Fecha DATE, CodigoCliente INT, Incremento INT UNSIGNED, 
CONSTRAINT FK_Clientes_ActualizacionLimeteCredito FOREIGN KEY (CodigoCliente) REFERENCES Clientes(CodigoCliente));
	INSERT INTO Info VALUES ((SELECT COUNT(*) FROM Clientes),(SELECT COUNT(*) FROM Empleados),(SELECT COUNT(*) FROM Pedidos));
	
  END;

  |

DELIMITER ;
