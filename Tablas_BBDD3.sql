USE Jardineria;

DROP TABLE IF EXISTS Comisiones;
DROP TABLE IF EXISTS Facturas;
DROP TABLE IF EXISTS AsientoContable;

CREATE TABLE Facturas (
    CodigoCliente INT,
    BaseImponible NUMERIC(15,2),
    IVA NUMERIC(15,2),
    Total NUMERIC(15,2),
    CodigoFactura INT,

    CONSTRAINT PK_Facturas PRIMARY KEY (CodigoFactura),
    CONSTRAINT FK_Clientes_Facturas FOREIGN KEY (CodigoCliente) REFERENCES Clientes(CodigoCliente)
);

CREATE TABLE Comisiones (
    CodigoEmpleado INT,
    Comision NUMERIC(15,2),
    CodigoFactura INT,

    CONSTRAINT FK_Empleados_Comisiones FOREIGN KEY (CodigoEmpleado) REFERENCES Empleados(CodigoEmpleado),
    CONSTRAINT FK_Facturas_Comisiones FOREIGN KEY (CodigoFactura) REFERENCES Facturas(CodigoFactura)
);

CREATE TABLE AsientoContable (
    CodigoCliente INT,
    DEBE NUMERIC(15,2),
    HABER NUMERIC(15,2),

    CONSTRAINT FK_Clientes_AsientoCOntable FOREIGN KEY (CodigoCliente) REFERENCES Clientes(CodigoCliente)
);

